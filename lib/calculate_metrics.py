import math
from collections import Counter
from datetime import datetime,timedelta
from config import config

#подсчет WRT для конкретной баги
def calculate_wrt_for_issue(issue, dc_dict):
    pass

#получить год и месяц из даты, например: '2017-2'
def _get_m_by_date(date: datetime):
    return "{}-{}".format(date.year, date.month)

#получить год и квартал из даты, например: '2017-Q1'
def _get_q_by_date(date: datetime):
    q = math.ceil(date.month / 3)
    return "{}-Q{}".format(date.year, q)



#получить первую и последнюю дату. Нужно чтобы понимать в каких рамках нам считать WRT. Если на определенный момент у нас
#еще нет или уже нет issue то нет смысла их пытаться считать на эту дату
def _get_timeline_ends(issues):
    # инициализируем переменные
    first_date = (datetime.now() + timedelta(days=1))
    end_date = (datetime.now() - timedelta(days=365 * 30))
    # находим значения для frst_date & last_date
    for issue in issues:
        if issue['start_date'] < first_date:
            first_date = issue['start_date']
        if issue['end_date'] > end_date:
            end_date = issue['end_date']
    return first_date, end_date

#главная функция подсчета
def calculate_wrt_timeline(issues, app_weight=None):
    result = list()



    return result






