from lib.load.github import load_from_github #грузим модуль для импорт данных github
from lib.upload.mongodb import upload_to_mongodb #грузим модуль для экспорта в mongo
from lib.calculate_metrics import calculate_wrt_timeline #грузим модуль для подсчета метрик
from config import config #грузим конфиг

#функция выгрузки данных.
def upload_data(timeline, db, collection):
    #если у в вас не mongo можно написать что-то свое и подставить тут
    upload_to_mongodb(timeline, db, collection)

#если включен импорт данных из github
if config['services']['github']['enabled'] == True:
    #загрузка всех issues
    print('[x] Выгрузка данных из GitHub')
    issues = load_from_github()
    #если в конфиге указано WRT: True
    if config['services']['metrics']['wrt']:
        #считаем wrt
        print('[x] Calculate wrt')
        wrt = calculate_wrt_timeline(issues)
        #выгружаем данные
        print('[x] Upload wrt to db')
        upload_data(wrt, config['services']['github']['upload_db'], 'wrt')

    # если в конфиге указано drw: True
    if config['services']['metrics']['drw']:
        wrt = calculate_wrt_timeline(issues)
        upload_data(wrt,config['services']['github']['upload_db'],'drw')

print('[x] Done')
